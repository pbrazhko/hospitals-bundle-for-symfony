<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 01.04.15
 * Time: 13:47
 */
namespace CMS\HospitalsBundle\Services;

use CMS\CoreBundle\AbstractCoreService;
use CMS\HospitalsBundle\Form\ServicesType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class ServicesService extends AbstractCoreService
{

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new ServicesType(),
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'HospitalsBundle:Services';
    }
}