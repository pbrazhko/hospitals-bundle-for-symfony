<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 25.02.16
 * Time: 11:38
 */

namespace CMS\HospitalsBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\HospitalsBundle\Form\DiseasesType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class DiseasesService extends AbstractCoreService
{
    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return [];
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new DiseasesType(),
            $data,
            [
                'data_class' => $this->getRepositoryClass()
            ]
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'HospitalsBundle:Diseases';
    }
}