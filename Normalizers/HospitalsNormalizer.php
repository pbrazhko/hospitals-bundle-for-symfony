<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 04.02.16
 * Time: 20:44
 */

namespace CMS\HospitalsBundle\Normalizers;

use CMS\HospitalsBundle\Entity\Hospitals;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class HospitalsNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param object $object object to normalize
     * @param string $format format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|bool|int|float|null
     */
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var Hospitals $object */
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'busy_hours_start' => $object->getBusyHoursStart(),
            'busy_hours_end' => $object->getBusyHoursEnd(),
            'days_off' => $object->getDaysOff(),
            'descriptions' => $object->getDescriptions(),
            'city' => $this->serializer->normalize($object->getCity(), $format, $context),
            'services' => $this->serializer->normalize($object->getServices(), $format, $context),
            'photos' => $this->serializer->normalize($object->getPhotos(), $format, $context),
            'rating' => $object->getRating()
        ];
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize.
     * @param string $format The format being (de-)serialized from or into.
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Hospitals;
    }
}