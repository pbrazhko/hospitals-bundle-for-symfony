<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 22.02.16
 * Time: 17:28
 */

namespace CMS\HospitalsBundle\Normalizers;


use CMS\HospitalsBundle\Entity\Services;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class ServicesNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param object $object object to normalize
     * @param string $format format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|bool|int|float|null
     */
    public function normalize($object, $format = null, array $context = array())
    {
        /**
         * @var Services $object
         */
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle()
        ];
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize.
     * @param string $format The format being (de-)serialized from or into.
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Services;
    }
}