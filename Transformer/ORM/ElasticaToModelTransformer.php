<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 04.12.15
 * Time: 10:41
 */

namespace CMS\HospitalsBundle\Transformer\ORM;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\Query;
use Elastica\Result;
use FOS\ElasticaBundle\Configuration\ConfigManager;
use FOS\ElasticaBundle\HybridResult;
use FOS\ElasticaBundle\Transformer\ElasticaToModelTransformerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class ElasticaToModelTransformer implements ElasticaToModelTransformerInterface
{

    private $configManager;

    private $registry;

    private $propertyAccessor;

    /**
     * ElasticaToModelTransformer constructor.
     * @param ConfigManager $configManager
     * @param Registry $registry
     */
    public function __construct(ConfigManager $configManager, Registry $registry)
    {
        $this->configManager = $configManager;
        $this->registry = $registry;

        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * Transforms an array of elastica objects into an array of
     * model objects fetched from the doctrine repository.
     *
     * @param array $elasticaObjects array of elastica objects
     *
     * @return array of model objects
     **/
    public function transform(array $elasticaObjects)
    {
        if (empty($elasticaObjects)) {
            return $elasticaObjects;
        }

        $identifiers = [];

        /** @var Result $elasticaObject */
        foreach ($elasticaObjects as $elasticaObject) {
            $data = $elasticaObject->getData();
            $identifiers[] = $this->propertyAccessor->getValue($data, '[' . $this->getIdentifierField() . ']');
        }

        $typeConfig = $this->configManager->getTypeConfiguration(
            $elasticaObject->getIndex(),
            $elasticaObject->getType()
        );

        return $this->findModelObjects($typeConfig->getModel(), $identifiers);
    }

    public function hybridTransform(array $elasticaObjects)
    {
        $indexedElasticaResults = array();
        foreach ($elasticaObjects as $elasticaObject) {
            $data = $elasticaObject->getData();
            $identifier = $this->propertyAccessor->getValue($data, '[' . $this->getIdentifierField() . ']');

            $indexedElasticaResults[$identifier] = $elasticaObject;
        }

        $objects = $this->transform($elasticaObjects);

        $result = array();
        foreach ($objects as $object) {
            $id = $this->propertyAccessor->getValue($object, $this->getIdentifierField());
            $result[] = new HybridResult($indexedElasticaResults[$id], $object);
        }

        return $result;
    }

    /**
     * Returns the object class used by the transformer.
     *
     * @return string
     */
    public function getObjectClass()
    {
        return 'CMS\HospitalsBundle\Entity\Hospitals';
    }

    /**
     * Returns the identifier field from the options.
     *
     * @return string the identifier field
     */
    public function getIdentifierField()
    {
        return 'id';
    }


    private function findModelObjects($entityName, $identifiers)
    {
        $repository = $this->registry->getManagerForClass($entityName)->getRepository($entityName);

        $builder = $repository->createQueryBuilder('m');

        $builder->where($builder->expr()->in('m.' . $this->getIdentifierField(), $identifiers));

        return $builder->getQuery()->getResult();
    }
}