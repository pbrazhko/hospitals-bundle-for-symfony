<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 22.02.16
 * Time: 17:21
 */

namespace CMS\HospitalsBundle\Provider;


use CMS\SearchBundle\Interfaces\SearchProviderInterface;
use CMS\SearchBundle\Services\SearchService;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;

class SearchHospitalsProvider implements SearchProviderInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $defaultFieldsData
     * @return mixed
     */
    public function buildFilterForm(FormBuilderInterface $builder, array $defaultFieldsData = array())
    {
        // TODO: Implement buildFilterForm() method.
    }

    /**
     * @param Form $form
     * @param SearchService $searchService
     * @return mixed
     */
    public function search(Form $form, SearchService $searchService)
    {
        // TODO: Implement search() method.
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'hospitals';
    }

}