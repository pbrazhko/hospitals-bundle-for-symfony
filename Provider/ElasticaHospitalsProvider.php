<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 22.02.16
 * Time: 17:21
 */

namespace CMS\HospitalsBundle\Provider;


use CMS\GalleryBundle\Normalizer\ImagesNormalizer;
use CMS\HospitalsBundle\Normalizers\HospitalsNormalizer;
use CMS\HospitalsBundle\Normalizers\ServicesNormalizer;
use CMS\HospitalsBundle\Services\HospitalsService;
use CMS\LocalizationBundle\Normalizer\CitiesNormalizer;
use Elastica\Document;
use Elastica\Exception\ElasticsearchException;
use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use Elastica\Type;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use FOS\ElasticaBundle\Provider\ProviderInterface;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Serializer\Serializer;

class ElasticaHospitalsProvider implements ProviderInterface
{
    /**
     * @var Type
     */
    private $hospitalsType;

    /**
     * @var HospitalsService
     */
    private $hospitalsService;

    /**
     * @var TransformedFinder
     */
    private $hospitalsFinder;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * ElasticaHospitalsProvider constructor.
     * @param Type $hospitalsType
     * @param HospitalsService $hospitalsService
     * @param TransformedFinder $hospitalsFinder
     * @param Logger $logger
     */
    public function __construct(Type $hospitalsType, HospitalsService $hospitalsService, TransformedFinder $hospitalsFinder, Logger $logger)
    {
        $this->hospitalsType = $hospitalsType;
        $this->hospitalsService = $hospitalsService;
        $this->hospitalsFinder = $hospitalsFinder;
        $this->logger = $logger;

        $normalizers = [
            new HospitalsNormalizer(),
            new ServicesNormalizer(),
            new CitiesNormalizer(),
            new ImagesNormalizer()
        ];

        $this->serializer = new Serializer($normalizers, ['']);
    }


    public function findById($hospitalId){
        $query = new BoolQuery();

        $fieldQuery = new Match();
        $fieldQuery->setField('id', $hospitalId);

        $query->addShould($fieldQuery);

        return $this->hospitalsFinder->findHybrid($query);
    }

    public function addHospitalToIndex($hospitalId){

        $results = $this->findById($hospitalId);

        if(!empty($results)){
            return $this->updateHospitalIndex($hospitalId);
        }

        if(null !== $hospital = $this->hospitalsService->findOneBy(['id' => $hospitalId])){
            try {
                $this->hospitalsType->addDocument(
                    (new Document())->setData(
                        $this->serializer->normalize($hospital)
                    )
                );
            }
            catch (ElasticsearchException $e){
                $this->logger->addCritical($e->getMessage());
                return;
            }
        }
    }

    public function updateHospitalIndex($hospitalId){

        $results = $this->findById($hospitalId);

        if(empty($results)){
            return $this->addHospitalToIndex($hospitalId);
        }

        /** @var HybridResult $result */
        foreach ($results as $result) {
            try {
                $this->hospitalsType->updateDocument(
                    (new Document($result->getResult()->getId()))->setData($this->serializer->normalize($result->getTransformed()))
                );
            }
            catch (ElasticsearchException $e){
                $this->logger->addCritical($e->getMessage());
                return;
            }
        }
    }

    /**
     * Persists all domain objects to ElasticSearch for this provider.
     *
     * @param \Closure $loggerClosure
     * @param array $options
     *
     * @return void
     */
    public function populate(\Closure $loggerClosure = null, array $options = array())
    {
        $hospitals = $this->hospitalsService->findBy(['is_published' => true, 'is_deleted' => false]);

        if($hospitals){
            $documents = [];

            foreach($hospitals as $hospital){
                $document = new Document();
                $document->setData(
                    $this->serializer->normalize($hospital, 'json')
                );

                $documents[] = $document;
            }

            $this->hospitalsType->addDocuments($documents);
        }
    }
}