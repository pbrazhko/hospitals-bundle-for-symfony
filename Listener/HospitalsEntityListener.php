<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 07.02.16
 * Time: 15:26
 */

namespace CMS\HospitalsBundle\Listener;


use CMS\HospitalsBundle\Entity\Hospitals;
use CMS\HospitalsBundle\Provider\ElasticaHospitalsProvider;
use Doctrine\ORM\Event\LifecycleEventArgs;

class HospitalsEntityListener
{
    /**
     * @var ElasticaHospitalsProvider
     */
    private $elasticaHospitalsProvider;

    /**
     * RestaurantsEntityListener constructor.
     * @param ElasticaHospitalsProvider $elasticaHospitalsProvider
     */
    public function __construct(ElasticaHospitalsProvider $elasticaHospitalsProvider)
    {
        $this->elasticaHospitalsProvider = $elasticaHospitalsProvider;
    }


    public function postPersist(LifecycleEventArgs $event){
        $entity = $event->getEntity();

        if(!$entity instanceof Hospitals){
            return;
        }

        $this->elasticaHospitalsProvider->addHospitalToIndex($entity->getId());
    }

    public function postUpdate(LifecycleEventArgs $event){
        $entity = $event->getEntity();

        if(!$entity instanceof Hospitals){
            return;
        }

        $this->elasticaHospitalsProvider->updateHospitalIndex($entity->getId());
    }
}