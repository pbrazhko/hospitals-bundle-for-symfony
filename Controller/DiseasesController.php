<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 25.02.16
 * Time: 11:38
 */

namespace CMS\HospitalsBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DiseasesController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $service = $this->get('cms.hospitals.diseases.service');

        return $this->render('HospitalsBundle:Diseases:list.html.twig', array(
            'diseases' => $service->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $service = $this->get('cms.hospitals.diseases.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->update($form->getData());

                return $this->redirect($this->generateUrl('cms_hospitals_diseases_list'));
            }
        }

        return $this->render('HospitalsBundle:Diseases:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.hospitals.diseases.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->update($form->getData());

                return $this->redirect($this->generateUrl('cms_hospitals_diseases_list'));
            }
        }

        return $this->render('HospitalsBundle:Diseases:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $service = $this->get('cms.hospitals.diseases.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_hospitals_diseases_list'));
    }
}