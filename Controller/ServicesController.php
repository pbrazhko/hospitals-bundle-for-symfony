<?php

namespace CMS\HospitalsBundle\Controller;

use CMS\HospitalsBundle\Entity\Services;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ServicesController
 * @package CMS\HospitalsBundle\Controller
 */
class ServicesController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $service = $this->get('cms.hospitals.services.service');

        return $this->render('HospitalsBundle:Services:list.html.twig', array(
            'services' => $service->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $service = $this->get('cms.hospitals.services.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->update($form->getData());

                return $this->redirect($this->generateUrl('cms_hospitals_services_list'));
            }
        }

        return $this->render('HospitalsBundle:Services:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.hospitals.services.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->update($form->getData());

                return $this->redirect($this->generateUrl('cms_hospitals_list'));
            }
        }

        return $this->render('HospitalsBundle:Services:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $service = $this->get('cms.hospitals.services.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_hospitals_services_list'));
    }
}
