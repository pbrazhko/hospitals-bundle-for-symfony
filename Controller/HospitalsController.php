<?php

namespace CMS\HospitalsBundle\Controller;

use CMS\HospitalsBundle\Entity\Hospitals;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class HospitalsController
 * @package CMS\HospitalsBundle\Controller
 */
class HospitalsController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $service = $this->get('cms.hospitals.service');

        return $this->render('HospitalsBundle:Hospitals:list.html.twig', array(
            'hospitals' => $service->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $service = $this->get('cms.hospitals.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->create($form->getData());

                return $this->redirect($this->generateUrl('cms_hospitals_list'));
            }
        }

        return $this->render('HospitalsBundle:Hospitals:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.hospitals.service');

        $form = $service->generateForm($service->findOneByid($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->create($form->getData());

                return $this->redirect($this->generateUrl('cms_hospitals_list'));
            }
        }

        return $this->render('HospitalsBundle:Hospitals:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $service = $this->get('cms.hospitals.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_hospitals_list'));
    }
}
