<?php

namespace CMS\HospitalsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HospitalsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'cms_localization_text_type', [
                'required' => false
            ])
            ->add('city', 'cms_localization_entity_type', array(
                'class' => 'LocalizationBundle:Cities',
                'property' => 'title',
                'empty_data' => null,
                'placeholder' => '',
            ))
            ->add('address')
            ->add('busy_hours_start')
            ->add('busy_hours_end')
            ->add('days_off', 'choice', array(
                    'choices' => array(
                        'mon' => 'Monday',
                        'tue' => 'Tuesday',
                        'wed' => 'Wednesday',
                        'thu' => 'Thursday',
                        'fri' => 'Friday',
                        'sat' => 'Saturday',
                        'sun' => 'Sunday',
                    ),
                    'multiple' => true,
                    'expanded' => true
                )
            )
            ->add('descriptions', 'cms_localization_textarea_type', [
                'required' => false
            ])
            ->add('is_published')
            ->add('is_deleted')
            ->add('services', 'cms_localization_entity_type', array(
                'class' => 'HospitalsBundle:Services',
                'property' => 'title',
                'multiple' => true,
                'expanded' => true
            ))
            ->add('diseases', 'cms_localization_entity_type', array(
                'class' => 'HospitalsBundle:Diseases',
                'property' => 'title',
                'multiple' => true,
                'expanded' => true
            ))
            ->add('photos', 'cms_gallery_images_type', [
                'required' => false
            ])
            ->add('location', 'cms_geo_geoobject_type');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\HospitalsBundle\Entity\Hospitals',
            'translation_domain' => 'labels'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cms_hospitalsbundle_hospitals';
    }
}
