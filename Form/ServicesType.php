<?php

namespace CMS\HospitalsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServicesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'cms_localization_text_type');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\HospitalsBundle\Entity\Services',
            'translation_domain' => 'labels'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cms_hospitalsbundle_services';
    }
}
