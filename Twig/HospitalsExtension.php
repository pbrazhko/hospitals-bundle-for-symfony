<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 23.02.15
 * Time: 19:40
 */

namespace CMS\HospitalsBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

class HospitalsExtension extends \Twig_Extension
{

    private $container;

    /** @var Twig_Environment */
    private $env;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('show_users_hospitals', array($this, 'showUsersHospitals'), array('is_safe' => array('html'), 'needs_environment' => true))
        );
    }

    public function showUsersHospitals(\Twig_Environment $environment, $userId, $template = 'HospitalsBundle:Twig:hospitals.html.twig')
    {
        $service = $this->container->get('cms.hospitals.service');

        $hospitals = array();
        $result = $service->findBy(array('create_by' => $userId));

        if ($result) {
            $hospitals = $result;
        }

        return $environment->render($template, array(
            'hospitals' => $hospitals
        ));
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'cms_hospitals_extension';
    }
}