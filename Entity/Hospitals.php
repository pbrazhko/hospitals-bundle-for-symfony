<?php

namespace CMS\HospitalsBundle\Entity;

use CMS\GeoBundle\Entity\Geoobjects;
use CMS\LocalizationBundle\Entity\Cities;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Hospitals
 */
class Hospitals
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var Cities
     */
    private $city;

    /**
     * @var Geoobjects
     */
    private $location;

    /**
     * @var string
     */
    private $address;

    /**
     * @var integer
     */
    private $busy_hours_start;

    /**
     * @var integer
     */
    private $busy_hours_end;

    /**
     * @var array
     */
    private $days_off;

    /**
     * @var Services
     */
    private $services;

    /**
     * @var Diseases
     */
    private $diseases;

    /**
     * @var string
     */
    private $descriptions;

    /**
     * @var Images
     */
    private $photos;

    /**
     * @var float
     */
    private $rating = 0.0;

    /**
     * @var integer
     */
    private $status = 0;

    /**
     * @var boolean
     */
    private $is_published;

    /**
     * @var boolean
     */
    private $is_deleted;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
        $this->services = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Restaurants
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Cities
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param Cities $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Geoobjects
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Geoobjects $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return int
     */
    public function getBusyHoursStart()
    {
        return $this->busy_hours_start;
    }

    /**
     * @param int $busy_hours_start
     * @return $this
     */
    public function setBusyHoursStart($busy_hours_start)
    {
        $this->busy_hours_start = $busy_hours_start;

        return $this;
    }

    /**
     * @return int
     */
    public function getBusyHoursEnd()
    {
        return $this->busy_hours_end;
    }

    /**
     * @param int $busy_hours_end
     * @return $this
     */
    public function setBusyHoursEnd($busy_hours_end)
    {
        $this->busy_hours_end = $busy_hours_end;

        return $this;
    }

    /**
     * @return array
     */
    public function getDaysOff()
    {
        return $this->days_off;
    }

    /**
     * @param array $days_off
     * @return $this
     */
    public function setDaysOff($days_off)
    {
        $this->days_off = $days_off;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param ArrayCollection $services
     * @return $this
     */
    public function setServices($services)
    {
        $this->services = $services;

        return $this;
    }

    /**
     * @return Diseases
     */
    public function getDiseases()
    {
        return $this->diseases;
    }

    /**
     * @param Diseases $diseases
     * @return $this
     */
    public function setDiseases($diseases)
    {
        $this->diseases = $diseases;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * @param string $descriptions
     * @return $this
     */
    public function setDescriptions($descriptions)
    {
        $this->descriptions = $descriptions;

        return $this;
    }

    /**
     * @return Images
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param Images $photos
     * @return $this
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;

        return $this;
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     *
     * @return Hospitals
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->is_published;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param boolean $is_published
     * @return $this
     */
    public function setIsPublished($is_published)
    {
        $this->is_published = $is_published;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * @param \DateTime $date_create
     * @return $this
     */
    public function setDateCreate($date_create)
    {
        $this->date_create = new \DateTime();

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param \DateTime $date_update
     * @return $this
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = new \DateTime();

        return $this;
    }

    /**
     * @return int
     */
    public function getCreateBy()
    {
        return $this->create_by;
    }

    /**
     * @param int $create_by
     * @return $this
     */
    public function setCreateBy($create_by)
    {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }

    /**
     * @param int $update_by
     * @return $this
     */
    public function setUpdateBy($update_by)
    {
        $this->update_by = $update_by;
        return $this;
    }
}
