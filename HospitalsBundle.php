<?php

namespace CMS\HospitalsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class HospitalsBundle extends Bundle
{
    public function isEnabled()
    {
        return true;
    }

    public function getDescription()
    {
        return array(
            array(
                'title' => 'Hospitals',
                'defaultRoute' => 'cms_hospitals_list'
            ),
            array(
                'title' => 'Services',
                'defaultRoute' => 'cms_hospitals_services_list'
            ),
            array(
                'title' => 'Diseases',
                'defaultRoute' => 'cms_hospitals_diseases_list'
            )
        );
    }
}
